const inventory = require('../inventory.js');

function sortCarModelsAlphabetically(inventory) {
  const carModels = inventory.map(car => car.car_model);
  carModels.sort();
  return carModels;
}

const sortedCarModels = sortCarModelsAlphabetically(inventory);

console.log("Car models sorted alphabetically:");
console.log(sortedCarModels);

module.exports = sortCarModelsAlphabetically;