const inventory = require('../inventory.js');

const carYearsArray = require('../problems/problem4.js')

function CarOlderThan2000(carYearsArray) {
  return carYearsArray.filter(year => year < 2000);
}

const oldCars = CarOlderThan2000(carYearsArray(inventory));
console.log('Cars older than 2000:');
console.log(oldCars);

module.exports = CarOlderThan2000;