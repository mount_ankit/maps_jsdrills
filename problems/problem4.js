const inventory = require('../inventory.js');

function allYears(inventory) {
  const years = inventory.map(car => car.car_year);
  return years;
}

const carYearsArray = allYears(inventory);


module.exports = allYears;