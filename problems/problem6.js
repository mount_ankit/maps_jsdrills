const inventory = require('../inventory.js');

function luxuryCars(inventory) {
  return inventory.filter(car => car.car_make === 'BMW' || car.car_make === 'Audi');
}

const BMWAndAudiArray = luxuryCars(inventory);
console.log(JSON.stringify(BMWAndAudiArray, null, 2));


module.exports = luxuryCars;