
const inventory = require('../inventory');


// Function to filter cars by their ID
function findCarById(id) {
  return inventory.filter(car => car.id === id);
}

const carIdToFind = 33;
const foundCars = findCarById(carIdToFind);
console.log(foundCars)
module.exports = findCarById;


