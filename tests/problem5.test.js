
const CarOlderThan2000 = require('../problems/problem4.js')
const allYears = require('../problems/problem4.js')
const inventory = require('../inventory.js')

describe("LastCar", () => {

    it("it will give the year of car made before 2000",() => {
        const oldCars = CarOlderThan2000(allYears(inventory));
        expect(oldCars).to.deep.equal(
            [
                1983, 1990, 1995, 1987, 1996,
                1997, 1999, 1987, 1995, 1994,
                1985, 1997, 1992, 1993, 1964,
                1999, 1991, 1997, 1992, 1998,
                1965, 1996, 1995, 1996, 1999
              ]
        )}

)});