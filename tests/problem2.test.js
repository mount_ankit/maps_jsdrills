
const findLastCar = require("../problems/problem2.js")
const inventory = require("../inventory.js")


describe("LastCar", () => {

    it("should reutrn name of car maker and car model ",() => {
        const Lastcar = findLastCar(inventory);
        expect(Lastcar).toEqual(
            [
                {
                  id: 50,
                  car_make: 'Lincoln',
                  car_model: 'Town Car',
                  car_year: 1999
                }
              ]
        )}

)});