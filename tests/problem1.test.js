const findCarById = require('../problems/problem1.js')


describe("findCarById", () => {
    it("should return car information for id 33", () => {
        
        const car33 = findCarById(33);
        expect(car33).toEqual([ { id: 33, car_make: 'Jeep', car_model: 'Wrangler', car_year: 2011 } ])
    });

});